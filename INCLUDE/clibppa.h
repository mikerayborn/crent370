#ifndef CLIBPPA_H
#define CLIBPPA_H

#include "clibcrt.h"
#include "clibgrt.h"

typedef struct clibppa  CLIBPPA;

struct clibppa {
    char    ppaeye[4];          /* 00 Eye Catcher                       */
#define PPAEYE  "@PPA"          /* ...                                  */
    void    *ppaprev;           /* 04 Previous Save Area                */
    void    *ppanext;           /* 08 Next Save Area                    */
    CLIBCRT **ppacrt;           /* 0C C Runtime Library Anchor (Task)   */
    CLIBGRT *ppagrt;            /* 10 C Runtime Global Anchor (AS)      */
    void    *ppasave;           /* 14 saved "next" from TCBFSA          */
    void    *PPA18;             /* 18 unused                            */
    void    *PPA1C;             /* 1C unused                            */
};

CLIBPPA * __PPAGET(void);
CLIBPPA * __ppaget(void);

#endif
