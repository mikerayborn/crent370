#ifndef CLIBSSCT_H
#define CLIBSSCT_H
#include "clibssvt.h"

typedef struct ssct     SSCT;   /* subsystem CVT, SP=241 KEY=0              */

struct ssct {
    char        ssctid[4];      /* 00 CL4'SSCT' CONTROL BLOCK IDENTIFIER    */
    SSCT        *ssctscta;      /* 04 PTR TO NEXT SSCVT OR ZERO             */
    char        ssctsnam[4];    /* 08 SUBSYSTEM NAME                        */
    char        ssctflg1;       /* 0C FLAGS                                 */
#define SSCTSFOR    0x80        /* ... SERIAL FIB OPERATIONS REQUIRED       */
#define SSCTUPSS    0x40        /* ... USE PRIMARY SUBSYSTEM'S
                                       SERVICES FOR THIS SUBSYSTEM          */
    char        ssctssid;       /* 0D SUBSYSTEM IDENTIFIER. SET BY
                                      SUBSYSTEM FIRST TIME IT IS
                                      INVOKED AFTER IPL                     */
#define SSCTUNKN    0x00        /* ... UNKNOWN SUBSYSTEM ID                 */
#define SSCTJES2    0x02        /* ... JES2 SUBSYSTEM ID                    */
#define SSCTJES3    0x03        /* ... JES3 SUBSYSTEM ID                    */
    char        ssctrsv1[2];    /* 0E RESERVED                              */
    SSVT        *ssctssvt;      /* 10 SUBSYSTEM VECTOR TABLE POINTER        */
    void        *ssctsuse;      /* 14 RESERVED FOR SUBSYSTEM USAGE          */
};

#endif
