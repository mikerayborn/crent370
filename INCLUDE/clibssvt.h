#ifndef CLIBSSVT_H
#define CLIBSSVT_H

typedef struct ssvt     SSVT;       /* subsystem vector table               */

struct ssvt {
    short       ssvtrsv1;           /* 00 RESERVED                          */
    short       ssvtfnum;           /* 02 NUMBER OF FUNCTIONS SUPPORTED BY
                                          THIS SUBSYSTEM                    */
/*
**  256 BYTE FUNCTION MATRIX -
**
**    THE SSOB FUNCTION ID MINUS ONE IS USED AS AN OFFSET INTO
**    THIS MATRIX.
**
**        MATRIX FUNCTION BYTE  =0 : THE FUNCTION SPECIFIED IN THE
**                                   SSOB IS NOT SUPPORTED BY THIS
**                                   SUBSYSTEM.
**        MATRIX FUNCTION BYTE �=0 : THE VALUE (FUNCTION BYTE-1)*4
**                                   IS ADDED TO THE ADDRESS OF
**                                   SSVTFRTN TO OBTAIN THE
**                                   ADDRESS OF THE WORD CONTAINING
**                                   THE FUNCTION ROUTINE POINTER FOR
**                                   THIS REQUEST.
*/
    char        ssvtfcod[256];      /* 04 FUNCTION MATRIX                   */
    void        *ssvtfrtn[0];       /* 104 SSVTFRTN IS THE FIRST WORD OF A
                                           VARIABLE LENGTH MATRIX CONTAINING
                                           FUNCTION ROUTINE POINTERS FOR
                                           FUNCTIONS SUPPORTED BY THIS
                                           SUBSYSTEM.  THE MATRIX CAN BE A
                                           MAXIMUM OF 256 WORDS LONG.       */
};

#endif
