#ifndef CLIBOS_H
#define CLIBOS_H

/* __cs() compare and swap in new_value to memory, returns old_value */
unsigned __cs(void *mem, unsigned new_value);

/* __uinc() unsigned increment of memory, returns old value, wraps new value to 0 if old value is max unsigned */
unsigned __uinc(void *mem);

/* __udec() unsigned decrement of memory, returns old value, wraps new value to max unsigned if old value is 0 */
unsigned __udec(void *mem);

/* __inc() signed increment of memory, returns old value, wraps new value to 0 if old value is max signed */
int __inc(void *mem);

/* __dec() signed decrement of memory, returns old value, wraps new value to max signed if old value is min signed */
int __dec(void *mem);

#endif /* !CLIBOS_H */
