# crent370

Reentrant C Library for legacy MVS 3.8J operating system using GCCMVS.3.2.3 cross platform compiler.

#### Directories
```
ASM       Source for assembly routines that interface with C functions.
bin       Compiler and command scripts to upload the assembly files to MVS running on the Hercules emulator.
CLIB      C library routines (reentrant code/thread safe).
DYN75     C functions to interface with the dyn75 socket instructions (hack to native/host sockets).
INCLUDE   C include files.
MACLIB    Assembly macros and copy members (used on the mvs side for assembly).
RACF      C functions for accessing RACF/RAKF.  Must be running APF authorized.
TEST      C main routines for testing.
```
